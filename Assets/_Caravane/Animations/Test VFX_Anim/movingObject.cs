using UnityEngine;
using System.Collections;

public class movingObject : MonoBehaviour

{

    public float speed = 1.0f;

    void Update()
    {
        // Move the object forward along its z axis 1 unit/second.
        transform.Translate(0, 0, Time.deltaTime*-speed);
    }
}