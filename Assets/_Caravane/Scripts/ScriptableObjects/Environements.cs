///-----------------------------------------------------------------
///   Author : Maxence Deschamps                    
///   Date   : 25/01/2019 22:21
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.Puncho.CaravanePalace.Caravane {
    [CreateAssetMenu(fileName = "EnvironmentProperties", menuName = "Properties/Environment")]
    public class Environements : ScriptableObject {

        [System.Serializable]
        public struct Environment {
            [SerializeField] public GameObject[] sections;
            [SerializeField] public Material skyboxMaterial;

            [Header("Lightings")]
            [SerializeField] public Color lightColor;
            [SerializeField, ColorUsage(true, true)] public Color skyColor;
            [SerializeField, ColorUsage(true, true)] public Color equatorColor;
            [SerializeField, ColorUsage(true, true)] public Color groundColor;

            [Header("Fog")]
            [SerializeField] public Color fogColor;
            [SerializeField] public float fogDensity;
        }

        [SerializeField] private Environment[] environments;
        [SerializeField] private GameObject _transitionSection;

        public GameObject TransitionSection { get { return _transitionSection; } }

        public Environment GetEnvironment (int id) {
            return environments[id % environments.Length];
        }

        public GameObject[] GetSections (int id) {
            return environments[id % environments.Length].sections;
        }

        public GameObject GetRandomSection (int id) {
            GameObject[] sections = GetSections(id);

            return sections[Mathf.FloorToInt(Random.value * sections.Length)];
        }

        public int GetEnvironementsCount () {
            return environments.Length;
        }
    }
}



