///-----------------------------------------------------------------
///   Author : Maxence Deschamps                    
///   Date   : 27/01/2019 00:02
///-----------------------------------------------------------------

using Com.Puncho.CaravanePalace.Caravane.Controller;
using Com.Puncho.CaravanePalace.Caravane.UI;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Com.Puncho.CaravanePalace.Caravane {
    public class FPSMovement : MonoBehaviour {

        [Header("Controls")]
        [SerializeField] private float movementSpeed = 10;
        [SerializeField] private float jumpForce = 10;
        [SerializeField] private float lookSensitivity = 2;
        [SerializeField] private float movementDrag = 1;
        [SerializeField, Range(0f,1f)] private float airControl = 0.5f;

        [Header("View bobbing")]
        [SerializeField] private float bobbingIntensity = 1;
        [SerializeField] private float bobbingSpeed = 1;
        [SerializeField] private AnimationCurve viewBobbing;

        [Header("Slowmow Anim")]
        [SerializeField] private float duration = 1;
        [SerializeField] private AnimationCurve slowDown = AnimationCurve.Linear(0, 1, 1, 0);

        [Header("Slowmow")]
        [SerializeField] private float slowmoRegenAfter = 2;
        [SerializeField] private float slowmoConso = 1;

        //[Header("Crouch/Slide")]
        //[SerializeField] private float durationCrouch = 0.2f;
        //[SerializeField] private Camera _camera;
        //[SerializeField] private float durationSlide = 1f;
        //[SerializeField] private Vector3 crouchScale;

        [Header("Header")]
        [SerializeField] private Image slowmoBar;

        [Header("Event")]
        [SerializeField] private UnityEvent onDeath;

        private float countBobbing = 0;

        private float slowmoLeft = 1;
        private float lastUseSlowmoTime = 0;

        //private float elapsedTimeCrouch = 0;
        //private float ratioCrouch = 0;
        //private bool isSliding = false;
        //[SerializeField] private CapsuleCollider capsuleCollider;
        //private float defaultHeightCapsuleCollider;

        private Transform tf;
        private Rigidbody rb;
        new private Transform camera;

        private float cameraStartY;
        private bool isGrounded = true;
        public bool isDead = false;

        [SerializeField] private ParticleSystem ps;

        private RecapScreen recapScreen;

        #region MOBILE_BOOL
        protected bool _jumpInputMobile = false;
        public bool JumpInputMobile
        {
            set { _jumpInputMobile = value; }
        }

        protected bool _upInputMobile = false;
        public bool UpInputMobile
        {
            set { _upInputMobile = value; }
        }

        protected bool _BOTTOMInputMobile = false;
        public bool BottomInputMobile
        {
            set { _BOTTOMInputMobile = value; }
        }

        protected bool _LEFTInputMobile = false;
        public bool LeftInputMobile
        {
            set { _LEFTInputMobile = value; }
        }

        protected bool _RIGHTInputMobile = false;
        public bool RightInputMobile
        {
            set { _RIGHTInputMobile = value; }
        }

        protected float _horizontalValue = 0;
        public float HorizontalValue
        {
            set { _horizontalValue = value; }
        }

        protected float _verticalValue = 0;
        public float VerticalValue
        {
            set { _verticalValue = value; }
        }

        #endregion

        #region [INPUTS_TESTS]
        protected bool InputVertical()
        {
            return Input.GetButton("Vertical") || _upInputMobile || _BOTTOMInputMobile;
        }

        protected bool InputHorizontal()
        {
            return Input.GetButton("Horizontal") || _LEFTInputMobile || _RIGHTInputMobile;
        }

        protected float InputVerticalValue()
        {
            return (_upInputMobile || _BOTTOMInputMobile) ? _verticalValue : Input.GetAxis("Vertical");
        }

        protected float InputHorizontalValue()
        {
            return (_LEFTInputMobile || _RIGHTInputMobile) ? _verticalValue : Input.GetAxis("Horizontal");
        }

        protected bool InputJumpDown() {
            return Input.GetButtonDown("Jump") || _jumpInputMobile;
        }

        protected bool InputJump() {
            return Input.GetButton("Jump") || _jumpInputMobile;
        }
        #endregion

        #region [MONOBEHAVIOUR_FUNCTIONS]
        private void Start() {
            //capsuleCollider = GetComponent<CapsuleCollider>();
            //defaultHeightCapsuleCollider = capsuleCollider.height;

            recapScreen = GameObject.Find("Recap_Screen").GetComponent<RecapScreen>();
            tf = transform;
            rb = GetComponent<Rigidbody>();
            camera = Camera.main.transform;
            cameraStartY = camera.localPosition.y;

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void Update() {
            if (!isDead) {
                HandleSlowmo();
            }

            ViewBobbing();

            if (transform.position.y < -20 && !isDead) Death();


            if (InputJumpDown() && isGrounded) {
                rb.AddForce(tf.up * jumpForce, ForceMode.Acceleration);
            }

            if (!InputJump() && !isGrounded) {
                rb.AddForce(Vector3.down * Time.deltaTime * 1000, ForceMode.Acceleration);
            }

            //if (Input.GetButtonDown("Crouch"))
            //{
            //    isSliding = true;
            //    capsuleCollider.height = defaultHeightCapsuleCollider / 2;

            //    StartCoroutine(C_LerpCrouch(crouchScale, true));
            //}
        }

        //private IEnumerator C_LerpCrouch(Vector3 pTarget, bool isSlide = false)
        //{
        //    Vector3 startCrouchScale = transform.localScale;
        //    Vector3 endCrouchScale = pTarget;

        //    elapsedTimeCrouch = 0;
        //    ratioCrouch = 0;

        //    while (ratioCrouch < 1)
        //    {
        //        ratioCrouch = elapsedTimeCrouch / durationCrouch;
        //        elapsedTimeCrouch += Time.deltaTime;

        //        transform.localScale = Vector3.Lerp(startCrouchScale, endCrouchScale, ratioCrouch);

        //        yield return null;
        //    }

        //    if (isSliding) StartCoroutine(C_Slide());
        //    yield break;
        //}

        //private IEnumerator C_Slide()
        //{
        //    float elapsedTimeSlide = 0;
        //    float ratioSlide = 0;

        //    while (ratioSlide < 1)
        //    {
        //        ratioSlide = elapsedTimeSlide / durationSlide;
        //        elapsedTimeSlide += Time.deltaTime;

        //        Vector3 lCameraForward = _camera.transform.forward;
        //        lCameraForward.y = 0;

        //        Vector3 lCameraRight = _camera.transform.right;
        //        lCameraRight.y = 0;

        //        Vector3 newPos = lCameraForward * movementSpeed * Time.deltaTime;
        //        transform.position += newPos;

        //        yield return null;
        //    }
        //    isSliding = false;
        //    capsuleCollider.height = defaultHeightCapsuleCollider;
        //    StartCoroutine(C_LerpCrouch(Vector3.one));

        //    yield break;
        //}

        private void FixedUpdate () {
            RaycastHit hit;
            isGrounded = Physics.Raycast(tf.position, Vector3.down, out hit, 1.2f);

            if (InputJumpDown() && isGrounded) {
                rb.AddForce(tf.up * jumpForce, ForceMode.Acceleration);
            }

            if (!InputJump() && !isGrounded) {
                rb.AddForce(Vector3.down * Time.deltaTime * 100);
            }

            float control = movementSpeed * (isGrounded ? 1 : airControl);

            rb.AddForce(tf.forward * control * InputVerticalValue() * Time.deltaTime * 1000, ForceMode.Acceleration);
            rb.AddForce(tf.right * control * InputHorizontalValue() * Time.deltaTime * 500, ForceMode.Acceleration);


            float dragFactor = 1 - Time.deltaTime * (isGrounded ? movementDrag : 2.5f);
            rb.velocity = MultiplyV3(rb.velocity, new Vector3(dragFactor, 1f, dragFactor));

            tf.rotation = Quaternion.Euler(Vector3.up * Input.GetAxis("Mouse X") * lookSensitivity * Time.deltaTime * 100) * tf.rotation;

            if (isGrounded && !InputJump()) {
                _jumpInputMobile = false;
            }

            StickToGround();
        }

        private void OnCollisionEnter(Collision collision) {
            if (collision.collider.CompareTag("InstaKill")) {
                Death();
            } else {
                if (collision.collider.CompareTag("Kill") && collision.relativeVelocity.magnitude > 35f) {
                    Death();
                }
            }
        }

        #endregion

        private void StickToGround () {
            if (InputJump() || !isGrounded) return;

            RaycastHit hit;
            Physics.Raycast(tf.position, Vector3.down, out hit, 1.2f);

            Vector3 newPos = tf.position;
            newPos.y = hit.point.y + 1f;
            tf.position = newPos;
        }

        private Vector3 MultiplyV3 (Vector3 v1, Vector3 v2) {
            return new Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
        }


        private void HandleSlowmo() {
            if (Input.GetButton("Fire2") && slowmoLeft > 0) {
                slowmoLeft = Mathf.Max(0, slowmoLeft - Time.deltaTime * slowmoConso);
                lastUseSlowmoTime = Time.time;
            } else if (slowmoLeft < 1 && Time.time - lastUseSlowmoTime > slowmoRegenAfter) {
                slowmoLeft = Mathf.Min(1, slowmoLeft + Time.deltaTime * slowmoConso);
            }

            float targetTimeScale = Input.GetButton("Fire2") && slowmoLeft > 0 ? 0.4f : 1;
            SetTimeScale((Time.timeScale * 9 + targetTimeScale) / 10);

            slowmoBar.fillAmount = slowmoLeft;
        }


        public void Death () {
            if (isDead) return; 

            GetComponent<ScreenShake>().DoShake();
            isDead = true;
            MapScroller.instance.PlayerDeath();
            StartCoroutine(C_SlowToStop());
            recapScreen.Player_OnDeath();
            onDeath.Invoke();
            transform.GetChild(1).gameObject.SetActive(false);
            ps.Play();
        }

        private void SetTimeScale (float timeScale) {
            Time.timeScale = timeScale;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
        }

        private void ViewBobbing () {
            if ((InputVertical() || InputHorizontal()) && isGrounded) {
                countBobbing += Time.deltaTime * bobbingSpeed;
                Vector3 newPos = camera.localPosition;
                newPos.y = cameraStartY + viewBobbing.Evaluate(countBobbing % 1) * bobbingIntensity;
                camera.localPosition = newPos;
            }
        }

        private IEnumerator C_SlowToStop () {
            float ratio = 0;

            while (ratio < 1f) {
                SetTimeScale(slowDown.Evaluate(ratio));

                ratio += Time.fixedDeltaTime / duration;
                yield return null;
            }

            SetTimeScale(0);
        }
    }
}



