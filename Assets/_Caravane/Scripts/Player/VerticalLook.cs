///-----------------------------------------------------------------
///   Author : Maxence Deschamps                    
///   Date   : 27/01/2019 00:17
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.Puncho.CaravanePalace.Caravane.Player {
    public class VerticalLook : MonoBehaviour {

        [SerializeField] private float lookSensitivity = 2;

        private Transform tf;

        private float rotX = 0;

        private void Start() {
            tf = transform;
        }

        private void Update () {
            rotX -= Input.GetAxis("Mouse Y") * lookSensitivity * Time.deltaTime * 100;

            rotX = Mathf.Clamp(rotX, -80, 80);

            tf.localRotation = Quaternion.Euler(rotX, 0, 0);
        }
    }
}



