using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WavingObstacle : MonoBehaviour {

    [Header("Animation")]
	[SerializeField] private Texture[] frames;
    [SerializeField] private float animSpeed = 1;

    [Header("Pattern")]
    [SerializeField] private float movementAmplitude = 5;
    [SerializeField] private float movementSpeed = 1;
    [SerializeField] private float ejectionForce = 100;



    private Material mat;
    private Transform tf;
    private float animCount = 0;

    private bool dead = false;


    private void Start() {
        mat = GetComponent<Renderer>().material;
        tf = transform;
    }

    void Update () {
        if (dead) return;

        mat.mainTexture = frames[(int)animCount % frames.Length];

        tf.position += Vector3.right * movementSpeed * Time.deltaTime;


        if (Mathf.Abs(tf.position.x) > movementAmplitude) {
            tf.rotation = Quaternion.Euler(Vector3.up * (movementSpeed > 0 ? 0 : 180f));
            movementSpeed *= -1;
        }

        animCount += Time.deltaTime * animSpeed;
    }

    private void OnCollisionEnter(Collision collision) {
        Rigidbody rb = GetComponent<Rigidbody>();

        rb.useGravity = true;
        rb.isKinematic = false;

        rb.AddForce((tf.position - collision.transform.position).normalized * ejectionForce);
    }
}
