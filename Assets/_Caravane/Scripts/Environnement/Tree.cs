using Com.Puncho.CaravanePalace.Caravane;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    private Quaternion startRotation;
    private Quaternion endRotation;

    private float elapsedTime = 0;
    private float ratio = 0;
    [SerializeField] private float duration;
    [SerializeField] private AnimationCurve curve;
    [SerializeField] private GameObject CuttableTree;

    private float inverser = 1;


    private void Start () {

        if (transform.position.x > 0) inverser = -1;

        startRotation = transform.rotation;
        endRotation = startRotation * Quaternion.AngleAxis(-90 * inverser, Vector3.forward);
    }

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(C_Fall());
    }

    private IEnumerator C_Fall()
    {
        while (ratio < 1)
        {
            ratio = elapsedTime / duration;
            elapsedTime += Time.deltaTime;
            CuttableTree.transform.rotation = Quaternion.Slerp(startRotation, endRotation, curve.Evaluate(ratio));

            curve.Evaluate(ratio);

            yield return null;
        }

        yield break;
    }
}
