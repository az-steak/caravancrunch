///-----------------------------------------------------------------
///   Author : Maxence Deschamps                    
///   Date   : 26/01/2019 11:43
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.Puncho.CaravanePalace.Caravane.Environnement {
    public class TunnelEnd : MonoBehaviour {

        [SerializeField] private float minDistance = 10;
        [SerializeField] private float maxDistance = 100;

        private Material mat;
        private Color color;

        private Transform tf;
        private Transform player;

        private void Start () {
            tf = transform;
            player = Camera.main.transform;
            mat = GetComponent<Renderer>().material;
            color = mat.color;
        }

        private void Update () {
            float distance = Vector3.Distance(player.position, tf.position);

            color.a = Mathf.InverseLerp(minDistance, maxDistance, distance);
            mat.color = color;
        }
    }
}



