using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallRock : MonoBehaviour {

    private Rigidbody rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(Random.Range(-1f, 1f)*10, Random.Range(-1f, 1f)*10, Random.Range(-1f, 1f)*10, ForceMode.Impulse);
    }
}
