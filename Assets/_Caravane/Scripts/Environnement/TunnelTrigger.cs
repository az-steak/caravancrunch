///-----------------------------------------------------------------
///   Author : Maxence Deschamps                    
///   Date   : 26/01/2019 12:09
///-----------------------------------------------------------------

using Com.Puncho.CaravanePalace.Caravane.Controller;
using UnityEngine;

namespace Com.Puncho.CaravanePalace.Caravane.Environnement {
    public class TunnelTrigger : MonoBehaviour {


        private void OnTriggerEnter(Collider other) {
            if (other.CompareTag("Player")) {
                MapScroller.instance.ApplyProperties();
            }
        }
    }
}



