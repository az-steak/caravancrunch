using Com.Puncho.CaravanePalace.Caravane;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigRock : MonoBehaviour
{

    [SerializeField] private SmallRock smallRock;
    [SerializeField] private float impulseMin = 0;
    [SerializeField] private float impulseMax = 50;
    [SerializeField] private float maxLittleRocks = 10;

    private Rigidbody rb;
    private List<SmallRock> smallRocks = new List<SmallRock>();

    private void Start()
    {
        float randomImpulse = Random.Range(impulseMin, impulseMax);
        rb = GetComponent<Rigidbody>();
        rb.AddForce( 0, -randomImpulse,0, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        SpawnRocks(collision.contacts[0].point);
        return;
    }

    private void SpawnRocks (Vector3 spawnPoint)
    {
        float randomMax = Mathf.Round(Random.value * maxLittleRocks);
        for (int i = 0; i < randomMax; i++)
        {
            SmallRock newRock = Instantiate(smallRock);
            newRock.transform.position = spawnPoint + new Vector3(0, 0.5f, 0);
            smallRocks.Add(newRock);
        }
    }

    private void OnDestroy()
    {
        
        int i = smallRocks.Count - 1;
        while (i > 0)
        {
            if (smallRocks[i] != null) Destroy(smallRocks[i].gameObject);
            i--;
        }
        smallRocks.Clear();
    }
}
