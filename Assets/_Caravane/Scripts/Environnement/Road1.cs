using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road1 : MonoBehaviour {

    [SerializeField] private BigRock bigRock;
    [SerializeField] private Transform spawnRock;
    [SerializeField] private float rangeSpawn = 200;
    [SerializeField] private float maxRocks = 10;

    [SerializeField] private Tree treePrefab;

    private List<BigRock> bigRocks = new List<BigRock>();
    private List<Tree> trees = new List<Tree>();

    [SerializeField] private GameObject startPointLineSpawnTreesLeft;
    [SerializeField] private GameObject endPointLineSpawnTreesLeft;
    [SerializeField] private GameObject startPointLineSpawnTreesRight;
    [SerializeField] private GameObject endPointLineSpawnTreesRight;

    private void Start()
    {
        if(treePrefab !=null) SpawnTrees();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            SpawnRocks();
        }
    }

    private void SpawnRocks ()
    {
        float randomMax = Mathf.Round(Random.value * maxRocks);
        for (int i = 0; i < randomMax; i++)
        {
            BigRock newRock = Instantiate(bigRock);
            newRock.transform.position = spawnRock.position + new Vector3(0, 0, Random.Range(-rangeSpawn, rangeSpawn));
            bigRocks.Add(newRock);
        }
    }

    private void SpawnTrees()
    {

        float randomMaxLeft = Mathf.Round(Random.value * 1);
        float randomMaxRight = Mathf.Round(Random.value * 1);

        for (int i = 0; i < randomMaxLeft; i++)
        {
                Tree newTree = Instantiate(treePrefab);
                newTree.transform.position = new Vector3 (startPointLineSpawnTreesLeft.transform.position.x, startPointLineSpawnTreesLeft.transform.position.y+ 10, Random.Range(startPointLineSpawnTreesLeft.transform.position.z, endPointLineSpawnTreesLeft.transform.position.z));
                trees.Add(newTree);
        }

        for (int i = 0; i < randomMaxRight; i++)
        {
            Tree newTree = Instantiate(treePrefab);
            newTree.transform.position = new Vector3(startPointLineSpawnTreesRight.transform.position.x, startPointLineSpawnTreesRight.transform.position.y + 10, Random.Range(startPointLineSpawnTreesRight.transform.position.z, endPointLineSpawnTreesRight.transform.position.z));
            trees.Add(newTree);
        }
    }

    private void OnDestroy()
    {
        int i = bigRocks.Count - 1;
        while(i>0)
        {
            if(bigRocks[i]!=null)Destroy(bigRocks[i].gameObject);
            bigRocks.RemoveAt(i);
            i--;
        }
    }
}
