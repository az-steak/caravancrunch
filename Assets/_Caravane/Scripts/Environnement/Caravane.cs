///-----------------------------------------------------------------
///   Author : Maxence Deschamps                    
///   Date   : 26/01/2019 10:42
///-----------------------------------------------------------------

using Com.Puncho.CaravanePalace.Caravane.Controller;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Com.Puncho.CaravanePalace.Caravane.Environnement {
    public class Caravane : MonoBehaviour {
        [SerializeField] private float intensity = 1;
        [SerializeField] private float speed = 1;
        [SerializeField] private float targetDistance = 200;
        [SerializeField] private float deathDistance = 300;
        [SerializeField] private LayerMask groundLayer;
        [SerializeField] private Image UIArrow;
        [SerializeField] private Image UIArrowSecure;
        [SerializeField] private Image UIDot;

        [SerializeField] private GameObject door;
        [SerializeField] private GameObject wire;
        [SerializeField] private GameObject bike;

        private bool canOpenDoor = true;
        private float elapsedTimeOpenCloseDoor = 0;
        private float ratioOpenCloseDoor = 0;
        private float durationOpenCloseDoor = 0.5f;
        private Quaternion startQuaternionOpenCloseDoor;
        private bool isWaitingOpenDoor = false;


        [SerializeField] private AnimationCurve rollPattern;

        private float counter = 0;
        private Transform tf;
        private Vector3 lastPosArrow;
        private Vector3 lastForward;
        private float baseSpeed;

        private Transform player;
        private FPSMovement controller;


        [SerializeField] private AudioSource tireVSRoadSound;
    
        private void Start() {
            baseSpeed = speed;
            tf = transform;
            lastForward = tf.forward;
            
            lastPosArrow = UIArrow.transform.position;

            player = Camera.main.transform;
            controller = GameObject.FindGameObjectWithTag("Player").GetComponent<FPSMovement>();
        }

        private void Update () {
            float perlin = Mathf.PerlinNoise(counter, 0);
            float waving = perlin * 2 - 1;
            Vector3 newPos = tf.position + Vector3.forward * speed * Time.deltaTime;
            newPos.x = waving * intensity;
            Vector3 direction = newPos - tf.position;

            tf.position = newPos;
            tf.forward = (direction + lastForward * 9) / 10;

            lastForward = tf.forward;

            RaycastHit hit;

            if (Physics.Raycast(transform.position + Vector3.up * 5f, Vector3.down, out hit, 10, groundLayer)) {
                Vector3 position = hit.point;
                position.y = Mathf.Max(position.y, -3);
                tf.position = position;
                tf.up = Quaternion.Euler(Vector3.back * newPos.x * 5f * rollPattern.Evaluate(perlin)) * hit.normal;
            }

            float diffTargetDistance = Vector3.Distance(tf.position, player.position) - targetDistance;

            float distance = Mathf.Sqrt(Mathf.Pow(player.position.x - transform.position.x, 2) + Mathf.Pow(player.position.z - transform.position.z, 2));
            if (distance < 60 && canOpenDoor && MapScroller.HasPassedTunnel && !isWaitingOpenDoor )
            {
                StartCoroutine(C_OpenDoor());
            }

            speed = baseSpeed * Mathf.Clamp(1 - diffTargetDistance / 5f, 0.9f, 1.6f); 

            counter += Time.deltaTime;

            if (tf.position.z - player.position.z > deathDistance) {
                controller.Death();
            }

            UpdateArrow();
        }

        private float arrowAnimCount = 0;


        private Vector3 arrowTargetPosition;

        private void UpdateArrow() {
            Vector3 toCharDir = (tf.position - player.position).normalized;
            float angle = Vector3.Dot(player.forward, toCharDir);

            UIArrow.gameObject.SetActive(angle > 0 && !controller.isDead);

            UIArrow.sprite = transform.position.z - player.transform.position.z < 200 ? UIArrowSecure.sprite : UIDot.sprite;


            arrowTargetPosition = Camera.main.WorldToScreenPoint(tf.position) + Vector3.up * 100;
            UIArrow.transform.localScale = Vector3.one * 0.7f + new Vector3(Mathf.Cos(arrowAnimCount), Mathf.Sin(arrowAnimCount)) * 0.2f;


            UIArrow.transform.position = (UIArrow.transform.position * 5 + arrowTargetPosition) / 6f;

            lastPosArrow = UIArrow.transform.position;

            arrowAnimCount += Mathf.PerlinNoise(counter, 0) * Time.deltaTime * 10f;
        }


        #region [OBSTACLES_THROW]
        private IEnumerator C_OpenDoor()
        {
            canOpenDoor = false;
            isWaitingOpenDoor = true;

            startQuaternionOpenCloseDoor = door.transform.rotation;

            ratioOpenCloseDoor = 0;
            elapsedTimeOpenCloseDoor = 0;

            while (ratioOpenCloseDoor < 1)
            {
                OpenCloseDoor(180);
                yield return null;
            }

            GameObject lItemThrow = null;

            int lRandom = Random.Range(0, 3);

            switch (lRandom)
            {
                case 0:
                    lItemThrow = Instantiate(wire);
                    break;
                case 1:
                    lItemThrow = Instantiate(bike);
                    break;
                case 2:
                    break;
            }

            //GameObject lItemThrow = Random.Range(0, 3) == 0  ?  Instantiate(wire) : Instantiate(bike);

            if (lItemThrow != null)
            {
                Vector3 newPositionItem = transform.position;
                newPositionItem.y += 1;
                lItemThrow.transform.position = newPositionItem;
                tireVSRoadSound.Play();

            }
                StartCoroutine(C_CloseDoor());
            yield break;
        }

        private IEnumerator C_CloseDoor()
        {
            startQuaternionOpenCloseDoor = door.transform.rotation;
            ratioOpenCloseDoor = 0;
            elapsedTimeOpenCloseDoor = 0;

            while (ratioOpenCloseDoor < 1)
            {
                OpenCloseDoor(0);
                yield return null;
            }
            canOpenDoor = true;

            StartCoroutine(C_WaitBeforeReOpen() );
            yield break;
        }

        private IEnumerator C_WaitBeforeReOpen()
        {
            yield return new WaitForSeconds(4.0f);
            isWaitingOpenDoor = false;
        }

        private void OpenCloseDoor(int angleRotationToLerp)
        {
            elapsedTimeOpenCloseDoor += Time.deltaTime;
            ratioOpenCloseDoor = elapsedTimeOpenCloseDoor / durationOpenCloseDoor;

            door.transform.rotation = Quaternion.Slerp(startQuaternionOpenCloseDoor, Quaternion.AngleAxis(angleRotationToLerp, Vector3.up), ratioOpenCloseDoor);
        }
        #endregion
    }
}



