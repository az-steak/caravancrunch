using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bike : MonoBehaviour {

    [SerializeField] private float powerEjection;
	// Use this for initialization
	void Start () {
        transform.rotation = Random.rotation;
        GetComponent<Rigidbody>().AddForce(Vector3.forward * powerEjection);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
