using Com.Puncho.CaravanePalace.Caravane;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{

    [SerializeField] private float speed = 50f;
    [SerializeField] private float explosionForce = 100f;
    [SerializeField] private LayerMask groundLayer;

    private Transform tf;
    private Transform player;

    private bool exploded = false;


    private void Start()
    {
        tf = transform;
        player = Camera.main.transform;

        StartCoroutine(SpawnAnim());
    }

    // Update is called once per frame
    void Update () {
        if (!exploded) {
            transform.position -= Vector3.forward * Time.deltaTime * speed;
            RaycastHit hit;

            if (Physics.Raycast(transform.position + Vector3.up * 5f, Vector3.down, out hit, 10, groundLayer)) {
                transform.position = hit.point;
                transform.up = hit.normal;
            }
        }

        if (Vector3.Distance(transform.position, player.position) > 200 && (transform.position.z-player.position.z<0))
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Caravane") || other.CompareTag("InstaKill")) {
            exploded = true;
            Rigidbody rb = GetComponent<Rigidbody>();

            rb.isKinematic = false;
            Vector3 randomNoise = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * 100;

            rb.AddForce((Vector3.back + Vector3.up + randomNoise) / 2 * explosionForce);
            rb.AddTorque(randomNoise * Random.value * explosionForce);
        }
    }

    private IEnumerator SpawnAnim () {
        float size = 0;

        while (size < 1) {
            tf.localScale = Vector3.one * size;

            size += Time.deltaTime;
            yield return null;
        }

        tf.localScale = Vector3.one;
    }

}
