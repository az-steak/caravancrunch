using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tire : MonoBehaviour {

    [SerializeField] private float speed;
    [SerializeField] private float intensity;
    private Rigidbody rb;

    private void Start()
    {
        GetComponent<Rigidbody>().AddForce(Vector3.back * intensity);
    }
     //Update is called once per frame
    void Update () {
        transform.Translate( Vector3.back * speed * Time.deltaTime );
    }
}
