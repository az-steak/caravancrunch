using Com.Puncho.CaravanePalace.Caravane.Controller;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TunnelTriggerStart : MonoBehaviour {

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            MapScroller.instance.StopObstacleSpawn();
        }
    }
}
