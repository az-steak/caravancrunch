using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShake : MonoBehaviour {
    [SerializeField] private float duration;
    [SerializeField] private float speed;
    [SerializeField] private float intensity;
    [SerializeField] private AnimationCurve intensityPattern;

    private IEnumerator ShakeScreen() {
        Vector3 initialRotation = transform.rotation.eulerAngles;
        float ratio = 0;
        float counter = 0;
        while (ratio < 1) {
            counter += speed;
            float x = Mathf.PerlinNoise(counter, 0.1f);
            float y = Mathf.PerlinNoise(0.1f, counter);
            transform.rotation = Quaternion.Euler(new Vector3(x*2-1, y*2-1) * intensityPattern.Evaluate(ratio) * intensity + initialRotation);
            ratio += Time.unscaledDeltaTime / duration;
            yield return null;
        }
    }

    public void DoShake() {
        StartCoroutine("ShakeScreen");
    }
}
