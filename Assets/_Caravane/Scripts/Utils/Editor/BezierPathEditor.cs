using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BezierCurve))]
public class BezierPathEditor : Editor {

    private BezierCurve path;

    private void OnEnable() {
        path = (BezierCurve)target;
    }

    private void OnSceneGUI() {
        Vector3[] points = path.GetPoints();

        Handles.color = path.pathColor;

        for (int i = 0; i < points.Length; i++) {
            EditorGUI.BeginChangeCheck();
            Vector3 point = points[i];

            point = Handles.DoPositionHandle(point, Quaternion.identity);

            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(path, "Moved Path Point");
                EditorUtility.SetDirty(path);
                path.SetPoint(i, point);
            }


            if (i < points.Length - 1) {
                Vector3 nextPoint = points[i + 1];
                Handles.color = Color.grey;
                Handles.DrawLine(point, nextPoint);
            }
        }

        DrawCurve();
    }

    private void DrawCurve () {
        Vector3[] points = path.GetPoints();
        Vector3 startpoint = path.GetPoint(0);
        int lineStep = path.previewPathPrecision;

        Handles.color = path.pathColor;

        for (int i = 1; i <= lineStep; i++) {
            Vector3 lineEnd = path.GetPoint(i / (float)lineStep);
            Handles.DrawLine(startpoint, lineEnd);
            startpoint = lineEnd;
        }

    }
}
