///-----------------------------------------------------------------
///   Author : Sebastien RAYMONDAUD                    
///   Date   : 02/10/2018 13:41
///-----------------------------------------------------------------

using System;
using UnityEngine;

namespace Com.Puncho.CaravanePalace.Caravane.Utils {
    public class JsonHelper : MonoBehaviour {

        public static T[] GetJsonArray<T>(string json)
        {
            string newJson = "{ \"array\": " + json + "}";
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
            return wrapper.array;
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] array;
        }
    }
}



