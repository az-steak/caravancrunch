using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierCurve : MonoBehaviour {

    [Serializable]
    private struct BezierPoint {
        [SerializeField] public Vector3 position;

        public BezierPoint (Vector3 position) {
            this.position = position;
        }
    }


    [SerializeField] private bool worldSpace = true;

    [Header("Scene View")]
    [SerializeField] public Color pathColor = Color.white;
    [SerializeField, Range(2, 20)] public int previewPathPrecision = 2;


    private List<Vector3> points = new List<Vector3>();


    private void Reset() {
        points = new List<Vector3> { new Vector3(-1, 0), new Vector3(), new Vector3(1, 0) };
    }

    public Vector3[] GetPoints () {
        List<Vector3> positions = new List<Vector3>();
        
        foreach (Vector3 p in points) {
            positions.Add(worldSpace ? p : transform.TransformPoint(p));
        }

        return positions.ToArray();
    }

    public Vector3 GetPoint (float index) {
        Vector3 point = GetPoint(points[0], points[1], points[2], index);
        return worldSpace ? point : transform.TransformPoint(point);
    }

    private Vector3 GetPoint (Vector3 p1, Vector3 p2, Vector3 p3, float t) {
        return Vector3.Lerp(Vector3.Lerp(p1, p2, t), Vector3.Lerp(p2, p3, t), t);
    }

    public void SetPoint (int index, Vector3 position) {
        if (index < points.Count) {
            points[index] = worldSpace ? position : transform.InverseTransformPoint(position);
        }
    }
}
