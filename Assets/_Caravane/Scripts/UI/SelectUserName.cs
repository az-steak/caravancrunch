///-----------------------------------------------------------------
///   Author : Sebastien RAYMONDAUD                    
///   Date   : 26/01/2019 01:53
///-----------------------------------------------------------------

using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Com.Puncho.CaravanePalace.Caravane.UI {
    public class SelectUserName : MonoBehaviour {
        private static SelectUserName _instance;
        public static SelectUserName Instance { get { return _instance; } }

        private void Awake(){
            if (_instance){
                Destroy(gameObject);
                return;
            }

            _instance = this;
        }

        [SerializeField] protected InputField _inputUserName;
        [SerializeField] protected Button _validBtn;
        [SerializeField] protected GameObject _container;
        [SerializeField] protected GameObject _titleScreenContainer;
        [SerializeField] protected GameObject _waitScreen;
        [SerializeField] protected CaravaneAnimation _caravane;
        protected string _tempName;

        public static event Action<string> OnValidName;

        private void Start () {
            /*if (PlayerPrefs.GetString("Username") != string.Empty) {
                _tempName = PlayerPrefs.GetString("Username");
                _validBtn.onClick.Invoke();
                ValidBtn_OnClick();
            }
            */

            if (PlayerPrefs.GetString("Username", "") != "")
            {
                UserInformation_OnUserCreate();
                return;
            }

            _inputUserName.onValueChanged.AddListener(ChangeUserName);
            _validBtn.onClick.AddListener(ValidBtn_OnClick);
            UserInformation.OnUserCreate += UserInformation_OnUserCreate;
            _container.SetActive(true);
            _waitScreen.SetActive(false);

            _caravane.gameObject.SetActive(false);

        }

        public AudioClip _clicOn;
        public AudioClip _clicOff;
        public AudioSource _audioSource;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Return)) ValidBtn_OnClick();
        }

        public void PlaySoundOn()
        {
            _audioSource.PlayOneShot(_clicOn);
        }

        public void PlaySoundOff()
        {
            _audioSource.PlayOneShot(_clicOff);
        }

        private void UserInformation_OnUserCreate()
        {
            _container.SetActive(false);
            //_titleScreenContainer.SetActive(true);
            _caravane.gameObject.SetActive(true);
        }

        protected void ValidBtn_OnClick()
        {
            PlaySoundOn();

            if (_tempName == string.Empty)
            {
                Debug.LogError("player name empty");
                return;
            }

            if (OnValidName != null)
                OnValidName(_tempName);
            _waitScreen.SetActive(true);
        }

        protected void ChangeUserName(string pValue)
        {
            _tempName = pValue;
            PlayerPrefs.SetString("Username", _tempName);
        }

        private void OnDestroy(){
            if (this == _instance)
            {
                _instance = null;
                UserInformation.OnUserCreate -= UserInformation_OnUserCreate;
            }
        }

        protected void OnApplicationQuit()
        {
            PlayerPrefs.SetInt("viewAnimation", 0);
        }

    }
}



