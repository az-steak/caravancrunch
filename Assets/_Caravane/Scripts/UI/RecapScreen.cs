///-----------------------------------------------------------------
///   Author : Sebastien RAYMONDAUD                    
///   Date   : 26/01/2019 01:54
///-----------------------------------------------------------------

using Com.Puncho.CaravanePalace.Caravane.Controller;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Com.Puncho.CaravanePalace.Caravane.UI {
    public class RecapScreen : MonoBehaviour {
        private static RecapScreen _instance;
        public static RecapScreen Instance { get { return _instance; } }

        private void Awake(){
            if (_instance){
                Destroy(gameObject);
                return;
            }

            _instance = this;
        }

        [SerializeField] protected GameObject _container;
        [SerializeField] protected Text _scoreText;
        [SerializeField] protected Button _homeBtn;
        [SerializeField] protected Button _replayBtn;

        public AudioClip _clicOn;
        public AudioClip _clicOff;
        public AudioSource _audioSource;

        public void PlaySoundOn()
        {
            _audioSource.PlayOneShot(_clicOn);
        }

        public void PlaySoundOff()
        {
            _audioSource.PlayOneShot(_clicOff);
        }

        private void Start () {
            _homeBtn.onClick.AddListener(HomeBtn_OnClick);
            _replayBtn.onClick.AddListener(ReplayBtn_OnClick);
            _container.SetActive(false);
        }

        public void Player_OnDeath()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            _container.SetActive(true);
            _scoreText.text = "LANCE LE JEU DEPUIS UI CONNARD";

            if (UserInformation.Instance == null)
                return;
            UserInformation.Instance.CurrentScore = (int)MapScroller.instance.Score;
            _scoreText.text = "Distance parcourue : " + UserInformation.Instance.CurrentScore +  " m";
        }

        private void HomeBtn_OnClick () {
            PlaySoundOff();
            Time.timeScale = 1;
            SceneManager.LoadScene("UI");
        }

        private void ReplayBtn_OnClick()
        {
            PlaySoundOn();
            Time.timeScale = 1;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
        }

        private void OnDestroy(){
            if (this == _instance)
            {
                _instance = null;
                _homeBtn.onClick.RemoveListener(HomeBtn_OnClick);
                _replayBtn.onClick.RemoveListener(ReplayBtn_OnClick);
            }
        }
    }
}



