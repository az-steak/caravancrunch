///-----------------------------------------------------------------
///   Author : Sebastien RAYMONDAUD                    
///   Date   : 26/01/2019 04:48
///-----------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.Puncho.CaravanePalace.Caravane.UI {
    public class Waiting : MonoBehaviour {
        private static Waiting _instance;
        public static Waiting Instance { get { return _instance; } }

        private void Awake(){
            if (_instance){
                Destroy(gameObject);
                return;
            }

            _instance = this;
        }

        [SerializeField] protected List<Image> _listIcon = new List<Image>();
        float elapsedTime = 0;
        Color c;

        protected void Update()
        {
            elapsedTime += Time.deltaTime;
            for (int i = 0; i < _listIcon.Count; i++)
            {
                c = _listIcon[i].color;
                _listIcon[i].color = new Color(c.r, c.g, c.b, Mathf.Sin(elapsedTime - i));
            }
        }

        private void OnDestroy(){
            if (this == _instance)
                _instance = null;
        }
    }
}



