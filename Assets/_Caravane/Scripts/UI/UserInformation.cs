///-----------------------------------------------------------------
///   Author : Sebastien RAYMONDAUD                    
///   Date   : 26/01/2019 01:49
///-----------------------------------------------------------------

using System;
using UnityEngine;

namespace Com.Puncho.CaravanePalace.Caravane.UI {
    public class UserInformation : MonoBehaviour {
        private static UserInformation _instance;
        public static UserInformation Instance { get { return _instance; } }

        private void Awake(){
            if (_instance){
                Destroy(gameObject);
                return;
            }

            _instance = this;
            DontDestroyOnLoad(gameObject);
        }

        protected string _userName;
        public string UserName
        {
            get { return _userName; }
        }
        protected int _currentScore;
        public int CurrentScore
        {
            get { return _currentScore; }
            set
            {
                _currentScore = value;
                Leaderboard lLeaderBoard = FindObjectOfType<Leaderboard>();
                if (lLeaderBoard)
                    lLeaderBoard.UpdateScore(_currentScore);
            }
        }

        public static event Action OnUserCreate;

        private void Start () {
            _userName = PlayerPrefs.GetString("UserName", "");
        }

        public void SetUserName(string pUser)
        {
            Debug.Log(pUser);
            User temp = JsonUtility.FromJson<User>(pUser);

            _userName = temp.userName;
            PlayerPrefs.SetString("UserName", _userName);

            if (OnUserCreate != null)
                OnUserCreate();
        }

        private void OnDestroy(){
            if (this == _instance)
                _instance = null;
        }
    }

    [Serializable]
    public class User
    {
        public string _id;
        public string userName;
        public int score;
    }
}



