using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wobble : MonoBehaviour {

    [SerializeField] private float speed = 1;
    [SerializeField] private float intensity = 1;

    private Vector3 startPos;
    private Transform tf;
    private float seed;

	void Start () {
        tf = transform;
        startPos = tf.position;

        seed = Random.value * 10000;
    }
	

	void Update () {
        tf.position = startPos + new Vector3(Mathf.PerlinNoise(Time.time * speed + seed, 0), Mathf.PerlinNoise(0, Time.time * speed - seed)) * intensity;
	}
}
