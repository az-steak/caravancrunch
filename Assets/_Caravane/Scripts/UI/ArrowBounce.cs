using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowBounce : MonoBehaviour {

    private RectTransform rectTransform;
    private float elapsedTime = 0;
    [SerializeField] private float speedBounce = 50;

	// Use this for initialization
	void Start () {
        rectTransform = GetComponent<RectTransform>();
    }
	
	// Update is called once per frame
	void Update () {
        elapsedTime += Time.deltaTime * speedBounce;
        rectTransform.localPosition = new Vector3( 0, Mathf.Sin(elapsedTime), 0);
	}
}
