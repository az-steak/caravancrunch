///-----------------------------------------------------------------
///   Author : Sebastien RAYMONDAUD                    
///   Date   : 26/01/2019 01:53
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;

namespace Com.Puncho.CaravanePalace.Caravane.UI {
    public class TitleScreen : MonoBehaviour {
        private static TitleScreen _instance;
        public static TitleScreen Instance { get { return _instance; } }

        private void Awake(){
            if (_instance){
                Destroy(gameObject);
                return;
            }

            _instance = this;
        }

        [SerializeField] protected Button _playBtn;
        [SerializeField] protected Button _leaderboardBtn;
        [SerializeField] protected Button _optionBtn;
        [SerializeField] protected Button _quitBtn;

        [SerializeField] protected GameObject _container;
        protected GameObject _leaderBoardScreenContainer;
        [SerializeField] protected GameObject _optionScreenContainer;

        [SerializeField] protected OptionScreen optionScreen;
        [SerializeField] protected Leaderboard leaderboardScreen;
        [SerializeField] protected Cinematics cinematique;

        private void Start () {
            _container.SetActive(false);

            if (PlayerPrefs.GetInt("viewAnimation", 0) == 0)
            {
                PlayerPrefs.SetInt("viewAnimation", 1);
                StartAppear();
            }
        }

        public void ActiveBtn()
        {
            leaderboardScreen = Leaderboard.Instance;
            _leaderBoardScreenContainer = leaderboardScreen.transform.GetChild(0).gameObject;
            _playBtn.onClick.AddListener(PlayBtn_OnClick);    
            _leaderboardBtn.onClick.AddListener(LeaderboardBtn_OnClick);    
            _optionBtn.onClick.AddListener(OptionBtn_OnClick);    
            _quitBtn.onClick.AddListener(QuitBtn_OnClick);
        }

        public void StartAppear()
        {
            _container.SetActive(true);
            GetComponent<Animator>().SetBool("start", true);
            Debug.Log("Start Animation");
        }

        protected void PlayBtn_OnClick()
        {
            SelectUserName.Instance.PlaySoundOn();
            _container.SetActive(false);
            cinematique.StartCinematic();
        }

        protected void LeaderboardBtn_OnClick()
        {
            SelectUserName.Instance.PlaySoundOn();
            leaderboardScreen.Load();
            _container.SetActive(false);
            _leaderBoardScreenContainer.SetActive(true);
        }

        protected void OptionBtn_OnClick()
        {
            SelectUserName.Instance.PlaySoundOn();
            optionScreen.LoadInformation();
            _container.SetActive(false);
            _optionScreenContainer.SetActive(true);
        }

        protected void QuitBtn_OnClick()
        {
            SelectUserName.Instance.PlaySoundOff();
            Application.Quit();
        }

        private void OnDestroy(){
            if (this == _instance) {
                _playBtn.onClick.RemoveAllListeners();
                _leaderboardBtn.onClick.RemoveAllListeners();
                _optionBtn.onClick.RemoveAllListeners();
                _quitBtn.onClick.RemoveAllListeners();
                _instance = null;
            }
        }

    }
}



