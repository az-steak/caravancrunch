///-----------------------------------------------------------------
///   Author : Sebastien RAYMONDAUD                    
///   Date   : 26/01/2019 01:54
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace Com.Puncho.CaravanePalace.Caravane.UI {
    public class OptionScreen : MonoBehaviour {
        private static OptionScreen _instance;
        public static OptionScreen Instance { get { return _instance; } }

        private void Awake(){
            if (_instance){
                Destroy(gameObject);
                return;
            }

            _instance = this;
        }

        public string exposedNameMaster = "MasterVolume";
        public string exposedNameMusic = "MusicVolume";
        public string exposedNameSFX = "SFXVolume";

        [SerializeField] protected Button _backBtn;
        [SerializeField] protected Button _creditBtn;
        [SerializeField] protected GameObject _container;
        [SerializeField] protected GameObject _titleScreenContainer;
        [SerializeField] protected GameObject _creditScreenContainer;
        [SerializeField] protected AudioMixer audioMixer;

        [SerializeField] protected Slider _sliderMaster;
        [SerializeField] protected Slider _sliderMusic;
        [SerializeField] protected Slider _sliderSFX;

        private void Start()
        {
            _container.SetActive(false);
            _backBtn.onClick.AddListener(BackBtn_OnClick);
            _creditBtn.onClick.AddListener(CreditBtn_OnClick);

            _sliderMaster.onValueChanged.AddListener(Master_Changed);
            _sliderMusic.onValueChanged.AddListener(Music_Changed);
            _sliderSFX.onValueChanged.AddListener(SFX_Changed);

            LoadInformation();
        }

        public void LoadInformation()
        {
            _sliderMaster.value = PlayerPrefs.GetFloat(exposedNameMaster, 0);
            _sliderMusic.value = PlayerPrefs.GetFloat(exposedNameMusic, 0);
            _sliderSFX.value = PlayerPrefs.GetFloat(exposedNameSFX, 0);

            Master_Changed(_sliderMaster.value);
            Music_Changed(_sliderMusic.value);
            SFX_Changed(_sliderSFX.value);
        }

        public void Master_Changed(float pValue)
        {
            audioMixer.SetFloat(exposedNameMaster, pValue);
            PlayerPrefs.SetFloat(exposedNameMaster, pValue);
        }

        public void Music_Changed(float pValue)
        {
            audioMixer.SetFloat(exposedNameMusic, pValue);
            PlayerPrefs.SetFloat(exposedNameMusic, pValue);
        }

        public void SFX_Changed(float pValue)
        {
            audioMixer.SetFloat(exposedNameSFX, pValue);
            PlayerPrefs.SetFloat(exposedNameSFX, pValue);
        }

        protected void CreditBtn_OnClick()
        {
            SelectUserName.Instance.PlaySoundOn();
            _container.SetActive(false);
            _creditScreenContainer.SetActive(true);
        }

        protected void BackBtn_OnClick()
        {
            SelectUserName.Instance.PlaySoundOff();
            _container.SetActive(false);
            _titleScreenContainer.SetActive(true);
        }

        private void OnDestroy(){
            if (this == _instance)
                _instance = null;
        }
    }
}



