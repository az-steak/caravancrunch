using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour {

    [SerializeField] private float speed = 1;

    private Transform tf;

	void Start () {
        tf = transform;
	}
	
	// Update is called once per frame
	void Update () {
        tf.rotation = Quaternion.Euler(-tf.forward * speed * Time.deltaTime) * tf.rotation;
	}
}
