///-----------------------------------------------------------------
///   Author : Sebastien RAYMONDAUD                    
///   Date   : 26/01/2019 01:51
///-----------------------------------------------------------------

using Com.Puncho.CaravanePalace.Caravane.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Com.Puncho.CaravanePalace.Caravane.UI {
    public class Leaderboard : MonoBehaviour {
        private static Leaderboard _instance;
        public static Leaderboard Instance { get { return _instance; } }

        private void Awake(){
            if (_instance){
                Destroy(gameObject);
                return;
            }

            _instance = this;
            DontDestroyOnLoad(gameObject);
        }

        [SerializeField] protected Button _backBtn;
        [SerializeField] protected GameObject _container;
        [SerializeField] protected Transform _containerLeaderBoard;
        [SerializeField] protected GameObject _titleScreenContainer;
        [SerializeField] protected GameObject _infoPlayerPrefab;

        [SerializeField] private GameObject spinner;

        protected string url = "https://caravane-palace-ggj.herokuapp.com/";
        protected string createNewUserURL = "newUser";
        protected string getLeaderBoard = "getAllUsers";
        protected string getUser = "getUser";
        protected string updateBestScore = "updateBestScore?";
        protected string fieldName = "username";
        protected string fieldBestScore = "score";

        protected string _tempNameUser;

        private void Start () {
            _backBtn.onClick.AddListener(BackBtn_OnClick);
            SelectUserName.OnValidName += SelectUserName_OnValidName;
            _container.SetActive(false);
        }

        protected void BackBtn_OnClick()
        {
            SelectUserName.Instance.PlaySoundOff();
            _container.SetActive(false);
            _titleScreenContainer.SetActive(true);
        }

        private void SelectUserName_OnValidName(string pValue)
        {
            Debug.Log(pValue);
            _tempNameUser = pValue;
            CreateNewUser(pValue);
        }

        public void Load()
        {
            spinner.SetActive(true);

            if (_containerLeaderBoard.childCount != 0)
            {
                for (int i = 0; i < _containerLeaderBoard.childCount; i++)
                {
                    Destroy(_containerLeaderBoard.GetChild(i).gameObject);
                }
            }

            WWWForm form = new WWWForm();
            StartCoroutine(C_Server(url + getLeaderBoard, form, CreateLeaderBoard, ErrorServer));
        }

        public void UpdateScore(int pScore=0)
        {
            pScore = UnityEngine.Random.Range(1, 1000);
            Debug.Log(pScore);
            WWWForm form = new WWWForm();
            Debug.Log(UserInformation.Instance.UserName);
            form.AddField(fieldName, UserInformation.Instance.UserName);
            form.AddField(fieldBestScore, pScore);
            StartCoroutine(C_Server(url + updateBestScore, form, ValidBestScore, ErrorServer));
        }

        protected void ValidBestScore(string pValue)
        {

        }

        protected void CreateLeaderBoard(string pValue)
        {
            spinner.SetActive(false);
            User[] users = JsonHelper.GetJsonArray<User>(pValue);
            List<User> listUsers = new List<User>(users);

            listUsers.Sort((user1, user2) => (user2.score - user1.score));
            int index = 0;
            GameObject lInfoPlayer;

            foreach (var user in listUsers)
            {
                index++;
                lInfoPlayer = Instantiate(_infoPlayerPrefab);
                lInfoPlayer.transform.SetParent(_containerLeaderBoard, false);
                lInfoPlayer.transform.GetChild(0).GetComponentInChildren<Text>().text = index.ToString();
                lInfoPlayer.transform.GetChild(1).GetComponentInChildren<Text>().text = user.userName;
                lInfoPlayer.transform.GetChild(2).GetComponentInChildren<Text>().text = user.score.ToString();
            }
        }

        protected void CreateNewUser(string pValue)
        {
            WWWForm form = new WWWForm();
            form.AddField(fieldName, pValue);
            StartCoroutine(C_Server(url + createNewUserURL, form, ValidCreateNewPlayer, ErrorServer));
        }

        protected void ValidCreateNewPlayer(string pResult)
        {
            Debug.LogWarning(pResult);
            if (pResult == "already take")
            {
                Debug.Log("already take");
                GetUserName(_tempNameUser);
            }
            else
            {
                UserInformation.Instance.SetUserName(pResult);
            }
        }

        protected void GetUserName(string pValue)
        {
            WWWForm form = new WWWForm();
            form.AddField(fieldName, pValue);
            StartCoroutine(C_Server(url + getUser, form, UpdateUser, ErrorServer));
        }

        protected void UpdateUser(string pValue)
        {
            Debug.Log(pValue);
            string temp = pValue.Remove(pValue.Length - 1);
            Debug.Log(temp);
            temp = temp.Remove(0, 1);
            UserInformation.Instance.SetUserName(temp);
        }
        
        protected void ErrorServer(string pError)
        {
            Debug.LogError(pError);
        }

        protected IEnumerator C_Server(string pPath, Action<string> pCallBackValid, Action<string> pCallBackError)
        {
            using (UnityWebRequest www = UnityWebRequest.Get(pPath))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                    pCallBackError(www.error);
                else
                    pCallBackValid(www.downloadHandler.text);
            }
        }

        protected IEnumerator C_Server(string pPath, WWWForm pForm, Action<string> pCallBackValid, Action<string> pCallBackError)
        {
            using (UnityWebRequest www = UnityWebRequest.Post(pPath, pForm))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                    pCallBackError(www.error);
                else
                    pCallBackValid(www.downloadHandler.text);
            }
        }

        private void OnDestroy(){
            if (this == _instance)
            {
                _instance = null;
                SelectUserName.OnValidName -= SelectUserName_OnValidName;
            }

        }
    }
}



