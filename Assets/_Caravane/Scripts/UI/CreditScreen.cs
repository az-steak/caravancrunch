///-----------------------------------------------------------------
///   Author : Sebastien RAYMONDAUD                    
///   Date   : 26/01/2019 01:54
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;

namespace Com.Puncho.CaravanePalace.Caravane.UI {
    public class CreditScreen : MonoBehaviour {
        private static CreditScreen _instance;
        public static CreditScreen Instance { get { return _instance; } }

        private void Awake(){
            if (_instance){
                Destroy(gameObject);
                return;
            }

            _instance = this;
        }

        [SerializeField] protected Button _backBtn;
        [SerializeField] protected GameObject _container;
        [SerializeField] protected GameObject _optionScreenContainer;

        private void Start () {
            _container.SetActive(false);
            _backBtn.onClick.AddListener(BackBtn_OnClick);
        }

        protected void BackBtn_OnClick() {
            SelectUserName.Instance.PlaySoundOff();
            _container.SetActive(false);
            _optionScreenContainer.SetActive(true);
        }

        private void OnDestroy(){
            if (this == _instance)
                _instance = null;
        }
    }
}



