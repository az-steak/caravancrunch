///-----------------------------------------------------------------
///   Author : Sebastien RAYMONDAUD                    
///   Date   : 26/01/2019 16:23
///-----------------------------------------------------------------

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Com.Puncho.CaravanePalace.Caravane.UI {
    public class Cinematics : MonoBehaviour {
        private static Cinematics _instance;
        public static Cinematics Instance { get { return _instance; } }

        [SerializeField] private AudioSource musicCinematic;
        [SerializeField] protected bool _mobileVersion = false;

        private Action _doAction;
        public Action DoAction { get { return _doAction; } set { _doAction = value; } }

        private void Awake(){
            if (_instance){
                Destroy(gameObject);
                return;
            }

            DoAction = DoActionVoid;

            _instance = this;
        }

        private void DoActionVoid ()
        {

        }

        private void DoActionCinematic()
        {
            if (Input.GetMouseButtonDown(0)) StartLevel();
        }

        public void StartCinematic()
        {
            GetComponent<Animator>().SetBool("start", true);
            musicCinematic.Play();
            UserInformation.Instance.gameObject.GetComponent<AudioSource>().Stop();
            DoAction = DoActionCinematic;
        }

        private void Update()
        {
            DoAction();
        }

        public void StartLevel() {
            if (_mobileVersion)
                SceneManager.LoadScene("Main Mobile");
            else
                SceneManager.LoadScene("Main");
        }

        private void OnDestroy(){
            if (this == _instance)
                _instance = null;
        }
    }
}



