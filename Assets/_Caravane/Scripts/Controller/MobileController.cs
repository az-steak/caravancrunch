using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Com.Puncho.CaravanePalace.Caravane.Controller
{
    public class MobileController : MonoBehaviour {

        protected FPSMovement _player;
        protected FPSMovement Player
        {
            get
            {
                if (!_player)
                    _player = FindObjectOfType<FPSMovement>();
                return _player;
            }
        }

        public void OnPressUP(BaseEventData e)
        {
            Player.UpInputMobile = true;
            Player.VerticalValue = 1;
        }

        public void OnPressBottom(BaseEventData e)
        {
            Player.BottomInputMobile = true;
            Player.VerticalValue = -1;
        }

        public void OnPressLeft(BaseEventData e)
        {
            Player.LeftInputMobile = true;
            Player.HorizontalValue = 1;
        }

        public void OnPressRight(BaseEventData e)
        {
            Player.RightInputMobile = true;
            Player.HorizontalValue = -1;
        }


        public void OnReleaseUP(BaseEventData e)
        {
            Player.UpInputMobile = false;
            Player.VerticalValue = 0;
        }

        public void OnReleaseBottom(BaseEventData e)
        {
            Player.BottomInputMobile = false;
            Player.VerticalValue = 0;
        }

        public void OnReleaseLeft(BaseEventData e)
        {
            Player.LeftInputMobile = false;
            Player.HorizontalValue = 0;
        }

        public void OnReleaseRight(BaseEventData e)
        {
            Player.RightInputMobile = false;
            Player.HorizontalValue = 0;
        }

        public void OnPressJump(BaseEventData e) {
            Player.JumpInputMobile = true;
        }

        public void OnReleaseJump(BaseEventData e) {
            Player.JumpInputMobile = false;
        }
    }
}

