///-----------------------------------------------------------------
///   Author : Maxence Deschamps                    
///   Date   : 25/01/2019 22:08
///-----------------------------------------------------------------

using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Com.Puncho.CaravanePalace.Caravane.Controller {
    public class MapScroller : MonoBehaviour {

        [SerializeField] private Environements properties;
        [SerializeField] private float clipDistance = 200;
        [SerializeField] private float maxSpawnDistance = 150;
        [SerializeField] private float sectionLength = 1;
        [SerializeField] private float sectionsBetweenEnvironements = 6;

        [Header("Prefab")]
        [SerializeField] private GameObject playerPrefab;
        [SerializeField] private GameObject caravanePrefab;

        [Header("Car spawn")]
        [SerializeField] private GameObject[] cars;
        [SerializeField] private float minTimeBetweenSpawn = 1;

        private int environmentToUse = 0;
        private int envSectionsCount = 0;
        private bool _hasPassedTunnel = true;
        private float carSpawnTimeCount = 0;

        private float score = 0;
        public float Score
        {
            get { return score; }
        }

        private Text scoreText;
        private Light lightSource;

        public static MapScroller instance;
        public static bool HasPassedTunnel { get { return instance._hasPassedTunnel; } }

        public int EnvironmentToUse {
            get { return environmentToUse; }
            set {
                environmentToUse = value;
                mustSpawnTransition = true;
            } }

        private Transform player;
        private bool mustSpawnTransition = false;
        private List<Transform> sections = new List<Transform>();

        private bool _death = false;

        private void OnEnable() {
            if (instance != null) {
                Destroy(gameObject);
                return;
            }

            instance = this;
        }

        private void Start () {
            Transform section = Instantiate(properties.GetRandomSection(environmentToUse), Vector3.zero, Quaternion.identity).transform;
            sections.Add(section);

            player = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity).transform;
            Instantiate(caravanePrefab, Vector3.forward * 10, Quaternion.identity);
            scoreText = GameObject.FindGameObjectWithTag("ScoreText").GetComponent<Text>();
            lightSource = GameObject.FindGameObjectWithTag("Light").GetComponent<Light>();

            ApplyProperties();
        }

        public void PlayerDeath()
        {
            _death = true;
        }

        private void Update () {
            SpawnForward();
            Clip();
            SpawnCars();

            carSpawnTimeCount += Time.deltaTime;

            if (_death)
                return;

            score = Mathf.Max(score, player.position.z);
            scoreText.text = Mathf.Round(score).ToString();
            TestEnvironmentChange();
        }

        private void OnDestroy() {
            if (instance == this) {
                instance = null;
            }
        }

        private void SpawnForward() { 
            Transform lastSection = sections[sections.Count - 1];

            if (Vector3.Distance(lastSection.position, player.position) < maxSpawnDistance) {
                GameObject sectionPrefab;
                if (mustSpawnTransition) {
                    sectionPrefab = properties.TransitionSection;
                    mustSpawnTransition = false;
                } else {
                    sectionPrefab = properties.GetRandomSection(environmentToUse);
                    envSectionsCount++;
                }

                Vector3 position = lastSection.position + Vector3.forward * sectionLength;
                Transform section = Instantiate(sectionPrefab, position, Quaternion.identity).transform;

                sections.Add(section);
            }
        }

        private void Clip () {
            Transform section = sections[0];

            if (Vector3.Distance(section.position, player.position) > clipDistance) {
                sections.Remove(section);
                Destroy(section.gameObject);
            }
        }

        private void SpawnCars () {
            if (carSpawnTimeCount > minTimeBetweenSpawn && _hasPassedTunnel) {
                carSpawnTimeCount = 0;

                if (Random.value > Mathf.PerlinNoise(carSpawnTimeCount, 0f)) {
                    Vector3 position = sections[sections.Count - 3].position;
                    position.x = Random.Range(-5.5f, 5.5f);

                    Instantiate(cars[Mathf.FloorToInt(Random.value * cars.Length)], position, Quaternion.identity);
                }
            }
        }

        public void StopObstacleSpawn () {
            _hasPassedTunnel = false;
        }


        public void ApplyProperties () {
            _hasPassedTunnel = true;
            Com.Puncho.CaravanePalace.Caravane.Environements.Environment env = properties.GetEnvironment(environmentToUse);

            if (env.skyboxMaterial != null) {
                RenderSettings.skybox = env.skyboxMaterial;
            }

            StartCoroutine(C_LerpProperties(env));
        }

        private void TestEnvironmentChange () {
            if (envSectionsCount >= sectionsBetweenEnvironements) {
                envSectionsCount = 0;
                EnvironmentToUse = (EnvironmentToUse + 1) % properties.GetEnvironementsCount();
            }
        }

        private IEnumerator C_LerpProperties (Com.Puncho.CaravanePalace.Caravane.Environements.Environment env) {
            float ratio = 0;

            Color startSkyCol = RenderSettings.ambientSkyColor;
            Color endSkyCol = env.skyColor;

            Color startequatorColor = RenderSettings.ambientEquatorColor;
            Color endequatorColor = env.equatorColor;

            Color startgroundColor = RenderSettings.ambientGroundColor;
            Color endgroundColor = env.groundColor;

            Color startfogColor = RenderSettings.fogColor;
            Color endfogColor = env.fogColor;

            Color startLightColor = lightSource.color;
            Color endLightColor = env.lightColor;


            RenderSettings.fogDensity = env.fogDensity;

            while (ratio < 1) {
                RenderSettings.ambientSkyColor = Color.Lerp(startSkyCol, endSkyCol, ratio);
                RenderSettings.ambientEquatorColor = Color.Lerp(startequatorColor, endequatorColor, ratio);
                RenderSettings.ambientGroundColor = Color.Lerp(startgroundColor, endgroundColor, ratio);
                RenderSettings.fogColor = Color.Lerp(startfogColor, endfogColor, ratio);

                lightSource.color = Color.Lerp(startLightColor, endLightColor, ratio);

                ratio += Time.unscaledDeltaTime;
                yield return null;
            }
        }
    }
}



